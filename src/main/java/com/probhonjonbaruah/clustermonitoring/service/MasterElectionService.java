package com.probhonjonbaruah.clustermonitoring.service;
//package com.clustering.demo.service;
//
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.logging.log4j.message.FormattedMessage;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.clustering.demo.helper.AppConstants;
//import com.clustering.demo.helper.AppVariables;
//import com.clustering.demo.model.UrlModel;
//
//@Service
//public class MasterElectionService {
//	
//	static final Logger logger = LogManager.getLogger(MasterElectionService.class);
//	
//	@Autowired private AppVariables appVariable;
//	
//	@Autowired private AppConstants appConstant;
//	
//	
//	/**
//	 * a function to check if a new master is to be elected based on whether the 
//	 * current master is online/offline and the time for which the master is offline
//	 */
//	public void electMaster(Map<String, List<UrlModel>> urlMap) {
//		
//		/*
//		 * loop over all url groups
//		 */
//		Set<String> urlGroupSet = urlMap.keySet();
//		for (String urlGroup: urlGroupSet) {
//			
//			/*
//			 * store the master url
//			 */
//			UrlModel masterUrl = urlMap.get(urlGroup).get(0);
//			
//			/*
//			 * if offline
//			 */
//			if (masterUrl.getStatus().toLowerCase().compareTo(appConstant.getStatusKeyOffline()) == 0) {
//				
//				if (masterUrl.getTimeout() == 0 ) {
//					
//					if (logger.isDebugEnabled()) {
//						logger.debug("Scheduler", "masterElection", new FormattedMessage("url: {} timeout is 0", masterUrl));
//					}
//					
//					changePriority(urlMap.get(urlGroup));
//				} else {
//					
//					if (logger.isDebugEnabled()) {
//						logger.debug("Scheduler", "masterElection", new FormattedMessage("url: {} is offline, so decrement timeout", masterUrl));
//					}
//					
//					masterUrl.setTimeout(masterUrl.getTimeout() - 1);
//				}
//			
//			} else if (masterUrl.getStatus().toLowerCase().compareTo(appConstant.getStatusKeyOnline()) == 0) {
//				
//				if (logger.isDebugEnabled()) {
//					logger.debug("Scheduler", "masterElection", new FormattedMessage("url: {} is online, so reset timeout", masterUrl));
//				}
//				
//				masterUrl.setTimeout(appVariable.getTimeout());
//			} else {
//				masterUrl.setStatus(appConstant.getStatusKeyOffline());
//				logger.warn("urlGroup: {}; urlName: {}:{} wrong status type");
//			}
//		}
//	}
//	
//	/**
//	 * change priority order of the url list
//	 * @param urlPriorityList
//	 */
//	private void changePriority(List<UrlModel> urlPriorityList) {
//		int length = urlPriorityList.size()-1;
//		
//		/*
//		 * reset the timeout of the master url
//		 */
//		urlPriorityList.get(0).setTimeout(appVariable.getTimeout());
//		
//		/*
//		 * move the master to the back of the queue
//		 */
//		logger.info("Scheduler", "changePriority", new FormattedMessage("url: {} moved to back with queue due to timeout", urlPriorityList.get(0)));
//		urlPriorityList.add(urlPriorityList.get(0));
//		urlPriorityList.remove(0);
//		
//		/*
//		 * reset the timeout of the master url
//		 */
//		urlPriorityList.get(0).setTimeout(appVariable.getTimeout());
//		
//		
//		/*
//		 * repeat the above task for all offline url
//		 * till an online url is found or all urls are checked
//		 */
//		int count = 0;
//		while (urlPriorityList.get(0).getStatus().toLowerCase().compareTo(appConstant.getStatusKeyOffline())==0 && count < length) {
//			
//			urlPriorityList.get(0).setTimeout(appVariable.getTimeout());
//			
//			logger.info("Scheduler", "changePriority", new FormattedMessage("url: {} moved to back with queue due to offline", urlPriorityList.get(0)));
//			urlPriorityList.add(urlPriorityList.get(0));
//			urlPriorityList.remove(0);
//			
//			urlPriorityList.get(0).setTimeout(appVariable.getTimeout());
//			
//			count++;
//		}
//	}
//	
//}
