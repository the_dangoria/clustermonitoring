package com.probhonjonbaruah.clustermonitoring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableScheduling
@EnableSwagger2
@EnableAuthorizationServer
@EnableResourceServer
public class ClusteringMonitoringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClusteringMonitoringApplication.class, args);
	}

}
