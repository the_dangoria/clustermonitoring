package com.probhonjonbaruah.clustermonitoring.controller;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.probhonjonbaruah.clustermonitoring.exceptions.ClusterMonitoringApplicationException;
import com.probhonjonbaruah.clustermonitoring.helper.AppConstants;
import com.probhonjonbaruah.clustermonitoring.model.RESTFULResponseModel;
import com.probhonjonbaruah.clustermonitoring.model.UrlModel;
import com.probhonjonbaruah.clustermonitoring.model.UrlSetModel;
import com.probhonjonbaruah.clustermonitoring.service.RedisDAOService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@org.springframework.web.bind.annotation.RestController
@RequestMapping(path = "/${spring.api.resources.urlgroup}/{${spring.api.resources.urlgroup}}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@ResponseBody
@ResponseStatus(value = HttpStatus.OK)
@CrossOrigin("*")
@Api(tags = "urlgroup")
public class UrlGroupController {

	@Autowired private RedisDAOService redisDAOService;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.CREATED)
	@ApiOperation(value = "create an url group")
	public RESTFULResponseModel createUrlGroup(@PathVariable String urlgroup, @Valid @RequestBody UrlSetModel urlSetModel)
		throws ClusterMonitoringApplicationException {
		redisDAOService.createUrlGroup(urlgroup, urlSetModel.getUrlList());
		return new RESTFULResponseModel(AppConstants.RESOURCE_TYPE_URLGROUP, urlgroup, "created");
	}
	
	@GetMapping
	@ApiOperation(value = "retrieve all urls in an url group")
	public List<UrlModel> getUrlGroup(@PathVariable String urlgroup)
		throws ClusterMonitoringApplicationException {
		return redisDAOService.getUrlGroup(urlgroup);
	}
	
	@GetMapping(path = "/${spring.api.resources.url}")
	@ApiOperation(value = "retrieve a particular url in an url group")
	public UrlModel getParticularUrl(@PathVariable String urlgroup, @RequestParam(name = "url", required = true) String url, @RequestParam(name = "port", required = true) String port)
		throws ClusterMonitoringApplicationException {
		return redisDAOService.getUrl(urlgroup, url + ":" + port);
	}
		
	@PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	@ApiOperation(value = "update details of an particular url in an url group")
	public RESTFULResponseModel updateUrlGroup(@PathVariable String urlgroup, @Valid @RequestBody UrlSetModel urlSetModel)
		throws ClusterMonitoringApplicationException {
		redisDAOService.updateUrlGroup(urlgroup, urlSetModel.getUrlList());
		return new RESTFULResponseModel(AppConstants.RESOURCE_TYPE_URLGROUP, urlgroup, "updated");
	}

	@DeleteMapping
	@ApiOperation(value = "delete an url group")
	public RESTFULResponseModel deleteUrlGroup(@PathVariable String urlgroup)
		throws ClusterMonitoringApplicationException {
		redisDAOService.deleteUrlGroup(urlgroup);
		return new RESTFULResponseModel(AppConstants.RESOURCE_TYPE_URLGROUP, urlgroup, "deleted");
	}
	
	@DeleteMapping(path = "/${spring.api.resources.url}")
	@ApiOperation(value = "details some urls from an url group")
	public RESTFULResponseModel deleteUrlGroupSomeUrl(@PathVariable String urlgroup, @Valid @RequestBody UrlSetModel urlSetModel)
		throws ClusterMonitoringApplicationException {
		redisDAOService.deleteUrl(urlgroup, urlSetModel.getUrlList());
		return new RESTFULResponseModel(AppConstants.RESOURCE_TYPE_URL, urlgroup, "all requested urls deleted");
	}
}
