package com.probhonjonbaruah.clustermonitoring.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.message.FormattedMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.probhonjonbaruah.clustermonitoring.exceptions.ClusterMonitoringApplicationException;
import com.probhonjonbaruah.clustermonitoring.helper.AppConstants;
import com.probhonjonbaruah.clustermonitoring.model.UrlModel;
import com.probhonjonbaruah.clustermonitoring.repository.RedisCRUDRepositoryImpl;

/**
 * interaction layer between endpoints and database
 * 
 * tasks:
 * 		load details from database and store in RAM
 * 		perform CRUD operations taking business logic into account
 *  
 * @author probhonjon.baruah
 *
 */
@Service
public class RedisDAOService {
		
	@Autowired private RedisCRUDRepositoryImpl redisCRUDRepositoryImpl;
	
	/**
	 * create an urlGroup after checking whether the urlGroup and url 
	 * satisfy conditions
	 * 
	 * @param urlGroup
	 * @param urlModelList
	 * @throws ClusterMonitoringApplicationException 
	 */
	public void createUrlGroup(String urlGroup, List<UrlModel> urlModelList) throws ClusterMonitoringApplicationException {
		
		List<String> failedUrls = new ArrayList<>();
		
		/*
		 * check if urlGroup exists
		 */
		if (!redisCRUDRepositoryImpl.readAllUrlGroups().contains(urlGroup)) {
			/*
			 * loop through each url
			 */
			for (UrlModel urlModel: urlModelList) {
				/*
				 * check if url exists
				 */
				if (!redisCRUDRepositoryImpl.readAllUrls().contains(urlModel.getUrl())) {
					/*
					 * if above two conditions are met, create an url
					 */
					redisCRUDRepositoryImpl.createUrl(urlGroup, urlModel);
				} else {
					failedUrls.add(new FormattedMessage("Url: {} already exists.", urlModel.getUrl()).getFormattedMessage());
				}
			}
			
			
			if (!failedUrls.isEmpty()) {
				if (failedUrls.size() != urlModelList.size()) {
					throw new ClusterMonitoringApplicationException("4002", AppConstants.ERRORTYPE_DATABASE, failedUrls);
				} else {
					failedUrls.add(0, "All urls exists so urlGroup: " + urlGroup + " not created");
					throw new ClusterMonitoringApplicationException("4001", AppConstants.ERRORTYPE_DATABASE, failedUrls);
				}
			}
			
		} else {
			/*
			 * throw error
			 */
			throw new ClusterMonitoringApplicationException("4001", AppConstants.ERRORTYPE_DATABASE, new FormattedMessage("UrlGroup: {} already exists.", urlGroup));
		}
	}
	
	/**
	 * return a list of url that belongs to a urlGroup
	 * 
	 * @param urlGroup
	 * @return
	 * @throws ClusterMonitoringApplicationException 
	 */
	public List<UrlModel> getUrlGroup(String urlGroup) throws ClusterMonitoringApplicationException {
		Set<String> urlNameList = redisCRUDRepositoryImpl.readUrlGroup(urlGroup);
		
		List<UrlModel> urlList = new ArrayList<>();
		if (!urlNameList.isEmpty()) {
			for (String url: urlNameList) {
				urlList.add(redisCRUDRepositoryImpl.readUrl(url));
			}
		} else {
			/*
			 * throw err urlGroup does not exist
			 */
			throw new ClusterMonitoringApplicationException("4011", AppConstants.ERRORTYPE_DATABASE, new FormattedMessage("urlGroup: {} does not exist", urlGroup));
		}
		
		return urlList;
	}

	/**
	 * update an urlGroup after checking whether the urlGroup and url 
	 * satisfy conditions
	 * 
	 * @param urlGroup
	 * @param urlModelList
	 * @throws ClusterMonitoringApplicationException 
	 */
	public void updateUrlGroup(String urlGroup, List<UrlModel> urlModelList) throws ClusterMonitoringApplicationException {
		List<String> failedUrls = new ArrayList<>();
		
		/*
		 * check if urlGroup exists
		 */
		if (redisCRUDRepositoryImpl.readAllUrlGroups().contains(urlGroup)) {
			/*
			 * loop through each url
			 */
			for (UrlModel urlModel: urlModelList) {
				/*
				 * check if url exists in that group or does not exist at all
				 */
				if (redisCRUDRepositoryImpl.readUrlGroup(urlGroup).contains(urlModel.getUrl()) || !redisCRUDRepositoryImpl.readAllUrls().contains(urlModel.getUrl())) {
					/*
					 * if above two conditions are met, update an url
					 */
					redisCRUDRepositoryImpl.updateUrl(urlModel);
				} else {
					failedUrls.add("Url: " + urlModel.getUrl() + " already exists in another urlGroup.");
				}
			}
			
			/*
			 * throw exception for failed urls
			 */
			if (!failedUrls.isEmpty()) {
				throw new ClusterMonitoringApplicationException("4022", AppConstants.ERRORTYPE_DATABASE, failedUrls);
			}
		} else {
			/*
			 * throw error
			 */
			throw new ClusterMonitoringApplicationException("4001", AppConstants.ERRORTYPE_DATABASE, new FormattedMessage("UrlGroup: {} does not exists", urlGroup));
		}
	}
	
	/**
	 * delete an urlGroup and its related urls
	 * @throws ClusterMonitoringApplicationException 
	 */
	public void deleteUrlGroup(String urlGroup) throws ClusterMonitoringApplicationException {
		Set<String> urlNameList = redisCRUDRepositoryImpl.readUrlGroup(urlGroup);
		
		if (!urlNameList.isEmpty()) {
			/*
			 * for each url in urlGroup, delete the urlName
			 */
			for (String urlName: urlNameList) {
				redisCRUDRepositoryImpl.deleteUrl(urlGroup, urlName);
			}
		} else {
			/*
			 * throw err urlGroup does not exist
			 */
			throw new ClusterMonitoringApplicationException("4011", AppConstants.ERRORTYPE_DATABASE, new FormattedMessage("UrlGroup: {} does not exists", urlGroup));
		}
	}

	/**
	 * return a url that belongs to a urlGroup
	 * 
	 * @param urlGroup
	 * @param url
	 * @return
	 * @throws ClusterMonitoringApplicationException
	 */
	public UrlModel getUrl(String urlGroup, String url) throws ClusterMonitoringApplicationException {
		Set<String> urlGroupList = redisCRUDRepositoryImpl.readAllUrlGroups();
		
		/*
		 * does urlGroup exist
		 */
		if (urlGroupList.contains(urlGroup)) {
			/*
			 * does url exist in the urlGroup
			 */
			if (redisCRUDRepositoryImpl.readUrlGroup(urlGroup).contains(url)) {
				return redisCRUDRepositoryImpl.readUrl(url);
			} else {
				throw new ClusterMonitoringApplicationException("4012", AppConstants.ERRORTYPE_DATABASE, new FormattedMessage("url: {} does not exist in the urlgroup: {}", url, urlGroup));
			}
		} else {
			throw new ClusterMonitoringApplicationException("4011", AppConstants.ERRORTYPE_DATABASE, new FormattedMessage("urlgroup: {} does not exist", urlGroup));
		}
	}
	
	/**
	 * delete a list of urls from an urlGroup
	 * 
	 * @param urlGroup
	 * @param urlModelList
	 * @throws ClusterMonitoringApplicationException
	 */
	public void deleteUrl(String urlGroup, List<UrlModel> urlModelList) throws ClusterMonitoringApplicationException {
		List<String> failedUrls = new ArrayList<>();
		
		Set<String> urlGroupList = redisCRUDRepositoryImpl.readAllUrlGroups();
		
		/*
		 * does urlGroup exist
		 */
		if (urlGroupList.contains(urlGroup)) {
			/*
			 * does url exist in the urlGroup
			 */
			for (UrlModel urlModel: urlModelList) {
				if (redisCRUDRepositoryImpl.readUrlGroup(urlGroup).contains(urlModel.getUrl())) {
					redisCRUDRepositoryImpl.deleteUrl(urlGroup, urlModel.getUrl());
				} else {
					failedUrls.add("Url: " + urlModel.getUrl() + " does not exists.");
				}
			}
			
			if (!failedUrls.isEmpty()) {
				throw new ClusterMonitoringApplicationException("4012", AppConstants.ERRORTYPE_DATABASE, failedUrls);
			}
		} else {
			throw new ClusterMonitoringApplicationException("4011", AppConstants.ERRORTYPE_DATABASE, new FormattedMessage("urlgroup: {} does not exist", urlGroup));
		}
	}

}
