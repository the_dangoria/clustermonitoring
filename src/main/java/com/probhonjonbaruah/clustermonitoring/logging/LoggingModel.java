package com.probhonjonbaruah.clustermonitoring.logging;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LoggingModel {

	private Date timestamp;
	private String logLevel;
	private String threadName;
	private String className;
	private String message;
	
	public LoggingModel(Date timestamp, String logLevel, String threadName, String className, String message) {
		super();
		this.timestamp = timestamp;
		this.logLevel = logLevel;
		this.threadName = threadName;
		this.className = className;
		this.message = message;
	}

	public Date getTimestamp() {
		return timestamp;
	}
	
	public String getLogLevel() {
		return logLevel;
	}
	
	public String getThreadName() {
		return threadName;
	}
	
	public String getClassName() {
		return className;
	}
	
	public String getMessage() {
		return message;
	}
}
