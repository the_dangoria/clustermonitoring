package com.probhonjonbaruah.clustermonitoring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class UserDetailsConfig extends WebSecurityConfigurerAdapter {
		
	@Autowired
	CustomAuthorizationServerConfigurer customAuthorizationServerConfigurer;
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    	String password = new BCryptPasswordEncoder().encode("peter");
    	String password2 = new BCryptPasswordEncoder().encode("cgi");
    
    	
        auth.parentAuthenticationManager(customAuthorizationServerConfigurer.authenticationManager)
                .inMemoryAuthentication()
                .passwordEncoder(new BCryptPasswordEncoder())
                .withUser("Peter")
                .password(password)
                .roles("USER")
                .and()
                .withUser("Cgi")
                .password(password2)
                .roles("ADMIN", "USER");
    }

    @Bean
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
}

