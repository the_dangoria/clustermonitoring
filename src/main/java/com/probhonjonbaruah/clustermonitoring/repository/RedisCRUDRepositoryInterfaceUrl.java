package com.probhonjonbaruah.clustermonitoring.repository;

import com.probhonjonbaruah.clustermonitoring.model.UrlModel;

public interface RedisCRUDRepositoryInterfaceUrl {

	/**
	 * 1. add urlGroup to urlGroupSet
	 * 2. add url to urlGroup
	 * 3. add url as a hashset
	 * 
	 * @param urlGroup
	 * @param url
	 */
	public void createUrl(String urlGroup, UrlModel url);
	
	/**
	 * 1. read a url's info
	 * 
	 * @param urlName
	 * @return
	 */
	public UrlModel readUrl(String urlName);
			
	/**
	 * 1. update url details
	 * 
	 * update details about an url from an urlGroup
	 * allow only updates to status, priority and timeout
	 * @param urlGroup
	 * @param url 
	 */
	public void updateUrl(UrlModel url);
	
	/**
	 * 1. delete url as a hashset
	 * 2. delete url from urlGroup
	 * 
	 * @param urlGroup
	 * @param url
	 */
	public void deleteUrl(String urlGroup, String urlName);
	
}
