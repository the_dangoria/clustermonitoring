package com.probhonjonbaruah.clustermonitoring.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
public class CustomResourceServerConfig extends ResourceServerConfigurerAdapter {
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http
			.formLogin().disable()
			.httpBasic()
			.and()
			.authorizeRequests()
			.antMatchers("/api-docs", "/").permitAll()
			.antMatchers("/v2/api-docs", "/swagger-resources/configuration/ui", "/swagger-resources", "/swagger-resources/configuration/security", "/swagger-ui.html", "/swagger.json", "/webjars/**").permitAll()
			.antMatchers(HttpMethod.DELETE, "/urlgroup").hasAnyRole("ADMIN")
			.antMatchers("/**")
			.authenticated();
	}
}
