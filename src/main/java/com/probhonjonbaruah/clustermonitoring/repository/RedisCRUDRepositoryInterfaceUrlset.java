package com.probhonjonbaruah.clustermonitoring.repository;

import java.util.Set;

public interface RedisCRUDRepositoryInterfaceUrlset {
		
	/**
	 * 1. get info about all urls in an urlGroup
	 * 
	 * @param urlGroup
	 * @return
	 */
	public Set<String> readUrlGroup(String urlGroup);

	/**
	 * 1. delete all urls that are from the urlGroup as hashsets
	 * 2. delete urlGroup as a set
	 * 3. delete urlGroup from urlGroupSet
	 * 
	 * @param urlGroup
	 * @param url
	 */
	public void deleteUrlGroup(String urlGroup);
	
}
