package com.probhonjonbaruah.clustermonitoring.repository;

import static org.junit.Assert.*;

import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.probhonjonbaruah.clustermonitoring.helper.AppConstants;
import com.probhonjonbaruah.clustermonitoring.model.UrlModel;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestRedisCRUDRepositoryImpl {
	
	static final Logger logger = LogManager.getLogger(TestRedisCRUDRepositoryImpl.class);
	
	@Autowired private RedisCRUDRepositoryImpl redisCRUDRepositoryImpl;
	
	@Autowired private RedisTemplate<String, String> redisTemplate;

	
	@Before
	public void setUp() throws Exception {
		/*
		 * flush all db
		 */
		
		redisTemplate.getConnectionFactory().getConnection().flushDb();
	}
	
	@After
	public void tearDown() throws Exception {
		/*
		 * flush all db
		 */
		
		redisTemplate.getConnectionFactory().getConnection().flushDb();
	}

	private void constructDb() {
		
		redisTemplate.opsForHash().put("127.0.0.1:41001", "priority", "0");
		redisTemplate.opsForHash().put("127.0.0.1:41001", "status", "offline");
		redisTemplate.opsForHash().put("127.0.0.1:41001", "timeout", "10");
		redisTemplate.opsForSet().add("testGroup", "127.0.0.1:41001");
		redisTemplate.opsForSet().add(AppConstants.GLOBALURLGROUPSETNAME, "testGroup");
		
		redisTemplate.opsForHash().put("127.0.0.1:41003", "priority", "0");
		redisTemplate.opsForHash().put("127.0.0.1:41003", "status", "offline");
		redisTemplate.opsForHash().put("127.0.0.1:41003", "timeout", "10");
		redisTemplate.opsForSet().add("testGroup", "127.0.0.1:41003");
		redisTemplate.opsForSet().add(AppConstants.GLOBALURLGROUPSETNAME, "testGroup");
		
		redisTemplate.opsForHash().put("127.0.0.1:41002", "priority", "0");
		redisTemplate.opsForHash().put("127.0.0.1:41002", "status", "offline");
		redisTemplate.opsForHash().put("127.0.0.1:41002", "timeout", "10");
		redisTemplate.opsForSet().add("testGroup1", "127.0.0.1:41002");
		redisTemplate.opsForSet().add(AppConstants.GLOBALURLGROUPSETNAME, "testGroup1");
		
		redisTemplate.opsForSet().add(AppConstants.GLOBALURLSETNAME, "127.0.0.1:41001");
		redisTemplate.opsForSet().add(AppConstants.GLOBALURLSETNAME, "127.0.0.1:41002");
	}

	/**
	 * test read of all members of a url group set
	 */
	@Test
	public void test_readbyUrlGroupSet() {
		
		constructDb();

		Set<String> urlGroupList = redisCRUDRepositoryImpl.readAllUrlGroups();
		
		assertEquals(2, urlGroupList.size());
		assertTrue(urlGroupList.contains("testGroup"));
		assertTrue(urlGroupList.contains("testGroup1"));
	}
	
	/**
	 * test read of all members of a url group, when the group does not exits
	 */
	@Test
	public void test_readbyUrlGroupSet_nonexistent() {	
		Set<String> urlGroupList = redisCRUDRepositoryImpl.readAllUrlGroups();
		assertEquals(0, urlGroupList.size());
	}
	
	/**
	 * test scenario of reading all urls from the redis database
	 */
	@Test
	public void test_readAllUrls() {
		
		constructDb();

		Set<String> urlList = redisCRUDRepositoryImpl.readAllUrls();
		
		assertEquals(2, urlList.size());
		assertTrue(urlList.contains("127.0.0.1:41001"));
		assertTrue(urlList.contains("127.0.0.1:41002"));
	}
	
	/**
	 * test scenario when no urls are present in AllUrlSet
	 */
	@Test
	public void test_readAllUrls_nonexistent() {
		Set<String> urlList = redisCRUDRepositoryImpl.readAllUrls();
		assertEquals(0, urlList.size());
	}
	
	/**
	 * test flushing of db
	 */
	@Test
	public void test_flushdb() {
		
		constructDb();
		
		redisCRUDRepositoryImpl.flushdb();
		
		Set<String> keys = redisTemplate.keys("*");
		
		assertEquals(0, keys.size());
	}

	/**
	 * test creation of a new url in a urlGroup
	 */
	@Test
	public void test_create() {
		
		UrlModel urlModel = new UrlModel();
		urlModel.setPriority(0);
		urlModel.setStatus("offline");
		urlModel.setTimeout(10);
		urlModel.setUrl("127.0.0.1:41001");
		
		redisCRUDRepositoryImpl.createUrl("testGroup", urlModel);
		
		
		
		assertTrue(redisTemplate.hasKey(urlModel.getUrl()));
		assertEquals("offline", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYSTATUS));
		assertEquals("10", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYTIMEOUT));
		assertEquals("0", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYPRIORITY));
		
		assertTrue(redisTemplate.hasKey("testGroup"));
		assertEquals(1, redisTemplate.opsForSet().size("testGroup").intValue());
		assertTrue(redisTemplate.opsForSet().isMember("testGroup", urlModel.getUrl()));
		
		assertTrue(redisTemplate.hasKey(AppConstants.GLOBALURLGROUPSETNAME));
		assertEquals(1, redisTemplate.opsForSet().size(AppConstants.GLOBALURLGROUPSETNAME).intValue());
		assertTrue(redisTemplate.opsForSet().isMember(AppConstants.GLOBALURLGROUPSETNAME, "testGroup"));
		
		assertTrue(redisTemplate.hasKey(AppConstants.GLOBALURLSETNAME));
		assertEquals(1, redisTemplate.opsForSet().size(AppConstants.GLOBALURLSETNAME).intValue());
		assertTrue(redisTemplate.opsForSet().isMember(AppConstants.GLOBALURLSETNAME, "127.0.0.1:41001"));
	}
	
	/**
	 * test creation of a new url when it already exits,
	 * will replace
	 */
	@Test
	public void test_create_alreadyExits() {
		
		UrlModel urlModel = new UrlModel();
		urlModel.setPriority(0);
		urlModel.setStatus("offline");
		urlModel.setTimeout(10);
		urlModel.setUrl("127.0.0.1:41001");
		
		redisCRUDRepositoryImpl.createUrl("testGroup", urlModel);
		
		urlModel = new UrlModel();
		urlModel.setPriority(0);
		urlModel.setStatus("offline");
		urlModel.setTimeout(5);
		urlModel.setUrl("127.0.0.1:41001");
		
		redisCRUDRepositoryImpl.createUrl("testGroup", urlModel);
		
		
		
		assertTrue(redisTemplate.hasKey(urlModel.getUrl()));
		assertEquals("offline", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYSTATUS));
		assertEquals("5", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYTIMEOUT));
		assertEquals("0", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYPRIORITY));
		
		assertTrue(redisTemplate.hasKey("testGroup"));
		assertEquals(1, redisTemplate.opsForSet().size("testGroup").intValue());
		assertTrue(redisTemplate.opsForSet().isMember("testGroup", urlModel.getUrl()));
		
		assertTrue(redisTemplate.hasKey(AppConstants.GLOBALURLGROUPSETNAME));
		assertEquals(1, redisTemplate.opsForSet().size(AppConstants.GLOBALURLGROUPSETNAME).intValue());
		assertTrue(redisTemplate.opsForSet().isMember(AppConstants.GLOBALURLGROUPSETNAME, "testGroup"));
		
		assertTrue(redisTemplate.hasKey(AppConstants.GLOBALURLSETNAME));
		assertEquals(1, redisTemplate.opsForSet().size(AppConstants.GLOBALURLSETNAME).intValue());
		assertTrue(redisTemplate.opsForSet().isMember(AppConstants.GLOBALURLSETNAME, "127.0.0.1:41001"));
		
				
	}

	/**
	 * test basic read functionality
	 */
	@Test
	public void test_readbyUrl() {
		
		constructDb();
		
		UrlModel urlMap = redisCRUDRepositoryImpl.readUrl("127.0.0.1:41001");
		
		assertEquals("127.0.0.1:41001", urlMap.getUrl());
		assertEquals(0, urlMap.getPriority().intValue());
		assertEquals("offline", urlMap.getStatus());
		assertEquals(10, urlMap.getTimeout().intValue());
		
	}
	
	/**
	 * test basic read functionality when url does not exits
	 */
	@Test
	public void test_readbyUrl_nonexistent() {
		UrlModel urlMap = redisCRUDRepositoryImpl.readUrl("127.0.0.1:41001");
		
		assertNull(urlMap);
	}
	
	/**
	 * test update of a url details
	 */
	@Test
	public void test_update() {
		
		UrlModel urlModel = new UrlModel();
		urlModel.setPriority(0);
		urlModel.setStatus("offline");
		urlModel.setTimeout(10);
		urlModel.setUrl("127.0.0.1:41001");
		redisCRUDRepositoryImpl.createUrl("testGroup", urlModel);
		
		urlModel = new UrlModel();
		urlModel.setPriority(1);
		urlModel.setStatus("offline");
		urlModel.setTimeout(5);
		urlModel.setUrl("127.0.0.1:41002");
		redisCRUDRepositoryImpl.createUrl("testGroup", urlModel);
		
		urlModel = new UrlModel();
		urlModel.setPriority(0);
		urlModel.setStatus("offline");
		urlModel.setTimeout(5);
		urlModel.setUrl("127.0.0.1:41001");
		redisCRUDRepositoryImpl.updateUrl(urlModel);
		
		
		
		assertTrue(redisTemplate.hasKey("127.0.0.1:41001"));
		assertEquals("offline", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYSTATUS));
		assertEquals("5", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYTIMEOUT));
		assertEquals("0", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYPRIORITY));
		
		assertTrue(redisTemplate.hasKey("127.0.0.1:41002"));
		assertEquals("offline", redisTemplate.opsForHash().get("127.0.0.1:41002", AppConstants.KEYSTATUS));
		assertEquals("5", redisTemplate.opsForHash().get("127.0.0.1:41002", AppConstants.KEYTIMEOUT));
		assertEquals("1", redisTemplate.opsForHash().get("127.0.0.1:41002", AppConstants.KEYPRIORITY));
		
		assertTrue(redisTemplate.hasKey("testGroup"));
		assertEquals(2, redisTemplate.opsForSet().size("testGroup").intValue());
		assertTrue(redisTemplate.opsForSet().isMember("testGroup", "127.0.0.1:41001"));
		assertTrue(redisTemplate.opsForSet().isMember("testGroup", "127.0.0.1:41002"));
		
		assertTrue(redisTemplate.hasKey(AppConstants.GLOBALURLGROUPSETNAME));
		assertEquals(1, redisTemplate.opsForSet().size(AppConstants.GLOBALURLGROUPSETNAME).intValue());
		assertTrue(redisTemplate.opsForSet().isMember(AppConstants.GLOBALURLGROUPSETNAME, "testGroup"));
		
		assertTrue(redisTemplate.hasKey(AppConstants.GLOBALURLSETNAME));
		assertEquals(2, redisTemplate.opsForSet().size(AppConstants.GLOBALURLSETNAME).intValue());
		assertTrue(redisTemplate.opsForSet().isMember(AppConstants.GLOBALURLSETNAME, "127.0.0.1:41001"));
		assertTrue(redisTemplate.opsForSet().isMember(AppConstants.GLOBALURLSETNAME, "127.0.0.1:41002"));
		
		assertTrue(redisTemplate.opsForSet().members("testGroup").contains("127.0.0.1:41001"));
		assertTrue(redisTemplate.opsForSet().members("testGroup").contains("127.0.0.1:41002"));
	}

	/**
	 * test update of a url details when it does not exist
	 */
	@Test
	public void test_update_nonexistent() {
		
		UrlModel urlModel = new UrlModel();
		urlModel.setPriority(0);
		urlModel.setStatus("offline");
		urlModel.setTimeout(10);
		urlModel.setUrl("127.0.0.1:41001");
		redisCRUDRepositoryImpl.updateUrl(urlModel);
		
		assertEquals("10", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYTIMEOUT));
		assertEquals("offline", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYSTATUS));
		assertEquals("0", redisTemplate.opsForHash().get("127.0.0.1:41001", AppConstants.KEYPRIORITY));
	}

	/**
	 * test deleting an url
	 */
	@Test
	public void test_deleteUrl() {
		
		constructDb();
		
		redisCRUDRepositoryImpl.deleteUrl("testGroup", "127.0.0.1:41001");
		
		assertFalse(redisTemplate.hasKey("127.0.0.1:41001"));
		assertTrue(redisTemplate.hasKey("testGroup"));
		assertFalse(redisTemplate.opsForSet().members("testGroup").contains("127.0.0.1:41001"));
		assertFalse(redisTemplate.opsForSet().members(AppConstants.GLOBALURLSETNAME).contains("127.0.0.1:41001"));
		assertTrue(redisTemplate.opsForSet().members(AppConstants.GLOBALURLGROUPSETNAME).contains("testGroup"));	
	}
	
	/**
	 * test when trying to delete a url that does not exit
	 */
	@Test
	public void test_deleteUrl_nonExistent() {
		redisCRUDRepositoryImpl.deleteUrl("testGroup", "127.0.0.1:41001");
		
		assertTrue(true);
	}

	/**
	 * test read of all members of a url group
	 */
	@Test
	public void test_readbyUrlGroup() {

		constructDb();
		
		Set<String> urlList = redisCRUDRepositoryImpl.readUrlGroup("testGroup");
		
		assertTrue(urlList.contains("127.0.0.1:41001"));
		assertTrue(urlList.contains("127.0.0.1:41003"));
		assertEquals(2, urlList.size());
	}
	
	/**
	 * test read of all members of a url group, when the group does not exits
	 */
	@Test
	public void test_readbyUrlGroup_nonexistent() {	
		Set<String> urlList = redisCRUDRepositoryImpl.readUrlGroup("testGroup");
		assertEquals(0, urlList.size());
	}

	/**
	 * test deleting an url group
	 */
	@Test
	public void test_deleteUrlGroup() {
		constructDb();
		
		redisCRUDRepositoryImpl.deleteUrlGroup("testGroup");
		
		assertFalse(redisTemplate.hasKey("127.0.0.1:41001"));
		assertFalse(redisTemplate.hasKey("127.0.0.1:41003"));
		assertFalse(redisTemplate.hasKey("testGroup"));
		assertEquals(1, redisTemplate.opsForSet().members(AppConstants.GLOBALURLGROUPSETNAME).size());
		assertEquals(1, redisTemplate.opsForSet().members(AppConstants.GLOBALURLSETNAME).size());
	}
	
	/**
	 * test when trying to delete a url Group that does not exit
	 * no exception
	 */
	@Test
	public void test_deleteUrlGroup_nonExistent() {
		redisCRUDRepositoryImpl.deleteUrlGroup("testGroup");
	}
}
