package com.probhonjonbaruah.clustermonitoring.exceptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.message.FormattedMessage;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ClusterMonitoringApplicationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss Z")
	private final Date timestamp;
	
	private final String errorCode;
	
	private final String errorType;
	
	private final List<String> errorDescription;
	
	/**
	 * @param errorCode
	 * @param errorType
	 * @param errorDescription
	 */
	public ClusterMonitoringApplicationException(String errorCode, String errorType, List<String> errorDescription) {
		super();
		this.timestamp = new Date();
		this.errorCode = errorCode;
		this.errorType = errorType;
		this.errorDescription = errorDescription;
	}
	
	/**
	 * @param errorCode
	 * @param errorType
	 * @param errorDescription
	 */
	public ClusterMonitoringApplicationException(String errorCode, String errorType, FormattedMessage errorDescription) {
		super();
		this.timestamp = new Date();
		this.errorCode = errorCode;
		this.errorType = errorType;
		
		List<String> errorList = new ArrayList<>();
		errorList.add(errorDescription.getFormattedMessage());
		this.errorDescription = errorList;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorType() {
		return errorType;
	}

	public List<String> getErrorDescription() {
		return errorDescription;
	}

}
