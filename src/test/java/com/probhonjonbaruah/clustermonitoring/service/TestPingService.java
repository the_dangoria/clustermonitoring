package com.probhonjonbaruah.clustermonitoring.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.probhonjonbaruah.clustermonitoring.model.UrlModel;
import com.probhonjonbaruah.clustermonitoring.repository.RedisCRUDRepositoryImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestPingService {
	
	@MockBean private RedisCRUDRepositoryImpl redisCRUDRepositoryImpl;
	
	@SpyBean private PingService pingService;
	
	@LocalServerPort private int localport;
	
	private UrlModel url1;
	private UrlModel url2;
	
	@Before
	public void setup() {
				
		Set<String> urlSet = new TreeSet<>();
		
		url1 = new UrlModel();
		url1.setUrl("localhost:" + localport);
		url1.setTimeout(10);
		url1.setStatus("offline");
		url1.setPriority(0);
		
		url2 = new UrlModel();
		url2.setUrl("192.168.0.102:8081");
		url2.setTimeout(10);
		url2.setStatus("offline");
		url2.setPriority(0);
		
		urlSet.add(url1.getUrl());
		urlSet.add(url2.getUrl());
		
		when(redisCRUDRepositoryImpl.readAllUrls()).thenReturn(urlSet);
		doNothing().when(redisCRUDRepositoryImpl).updateUrl(any(UrlModel.class));
		when(redisCRUDRepositoryImpl.readUrl(url1.getUrl())).thenReturn(url1);
		when(redisCRUDRepositoryImpl.readUrl(url2.getUrl())).thenReturn(url2);
	}
	
	@Test
	public void test_ping() {

		pingService.ping();
		
		assertEquals("online", url1.getStatus());
		assertEquals("offline", url2.getStatus());
	}
}
