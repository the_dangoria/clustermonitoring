package com.probhonjonbaruah.clustermonitoring.service;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.FormattedMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.probhonjonbaruah.clustermonitoring.helper.AppConstants;
import com.probhonjonbaruah.clustermonitoring.model.UrlModel;
import com.probhonjonbaruah.clustermonitoring.repository.RedisCRUDRepositoryImpl;


/**
 * ping a list of urls
 * 
 * @author probhonjon.baruah
 * @date 2018.08.12
 */

@Component
@ConditionalOnProperty(prefix = "spring.application.scheduler", name="start", matchIfMissing=true)
public class PingService {
	
	static final Logger logger = LogManager.getLogger(PingService.class);
		
	@Autowired RedisCRUDRepositoryImpl redisCRUDRepositoryImpl;	
	
	/**
	 * given a set of urls ping the urls to detect whether OFFLINE or ONLINE
	 * 
	 * @param urllist
	 */
	@Scheduled(fixedRateString = "${spring.application.scheduler.ping.fixedrate}", initialDelayString = "${spring.application.scheduler.ping.initialdelay}")
	public void ping()  {
				
		Set<String> urlset = redisCRUDRepositoryImpl.readAllUrls();

		for(String url: urlset) {
			
			String[] urlsplitted = url.split(":");
			String ip = urlsplitted[0];
			int port = Integer.parseInt(urlsplitted[1]);

			Socket socket = new Socket();
			try {
				
				SocketAddress sockaddr = new InetSocketAddress(ip, port);
				socket.connect(sockaddr, 500);
				
				if (logger.isDebugEnabled()) {
					logger.debug(new FormattedMessage("{}:{} is {}", ip, port, AppConstants.KEYSTATUSONLINE));
				}

				setStatus(url, AppConstants.KEYSTATUSONLINE);
			} catch (IOException ex) {
				
				if (logger.isDebugEnabled()) {
					logger.debug(new FormattedMessage("{}:{} is {}", ip, port, AppConstants.KEYSTATUSOFFLINE));
				}
				
				setStatus(url, AppConstants.KEYSTATUSOFFLINE);		
			} finally {
				try {
					socket.close();
				} catch (IOException e) { 
					logger.warn("Socker closing io exception", e);
				}
			}
		}
	}
	
	private void setStatus(String url, String status) {
		UrlModel urlModel = redisCRUDRepositoryImpl.readUrl(url);
		urlModel.setStatus(status);
		redisCRUDRepositoryImpl.updateUrl(urlModel);
	}
}

