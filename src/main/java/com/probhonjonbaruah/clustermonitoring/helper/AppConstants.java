package com.probhonjonbaruah.clustermonitoring.helper;

public class AppConstants {
	
	/**
	 * 
	 */
	private AppConstants() {
		super();
	}

	/*
	 * database related
	 */
	public static final String GLOBALURLGROUPSETNAME = "globalurlgroupset";
	
	public static final String GLOBALURLSETNAME = "globalurlset";
			
	public static final String KEYPRIORITY = "priority";
	
	public static final String KEYTIMEOUT = "timeout";
	
	public static final String KEYSTATUS = "status";

	/*
	 * application related
	 */
	public static final String KEYSTATUSONLINE = "online";
	
	public static final String KEYSTATUSOFFLINE = "offline";
	
	public static final String RESOURCE_TYPE_URLGROUP = "urlgroup";
	
	public static final String RESOURCE_TYPE_URL = "url";
	
	public static final String RESOURCE_TYPE_PORT = "port";
	
	public static final String ERRORTYPE_DATABASE = "database";
	
	
	
	/*
	 * application.yaml related
	 */
	public static final String CONFIG_FILE = "application.yml";
	
	public static final String VARIABLE_REDIS_PREFIX = "redis";
	
	public static final String VARIABLE_REDIS_HOST = "host";
	
	public static final String VARIABLE_REDIS_PORT = "port";
	
	public static final String ENV_VARIABLE_REDISHOST = "redis_host";
	
	public static final String ENV_VARIABLE_REDISPORT = "redis_port";
}
