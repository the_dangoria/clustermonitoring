package com.probhonjonbaruah.clustermonitoring.model;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * a model that hold a urlgroup's name and its list of url
 * 
 * @author probhonjon.baruah
 * @date 2018.08.21
 */
public class UrlSetModel {
	
	@NotNull @Valid private List<UrlModel> urlList;

	public List<UrlModel> getUrlList() {
		return urlList;
	}

	public void setUrlList(List<UrlModel> urlList) {
		this.urlList = urlList;
	}

}
