package com.probhonjonbaruah.clustermonitoring.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Set;
import java.util.TreeSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.probhonjonbaruah.clustermonitoring.exceptions.ClusterMonitoringApplicationException;
import com.probhonjonbaruah.clustermonitoring.model.RESTFULResponseModel;
import com.probhonjonbaruah.clustermonitoring.repository.RedisCRUDRepositoryImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestActionsController {
	static final Logger logger = LogManager.getLogger(TestActionsController.class);
	
	@MockBean private RedisCRUDRepositoryImpl redisCRUDRepositoryImpl;
	
	@Autowired private TestRestTemplate testRestTemplate;
	
	@Value("${spring.api.version}") private String version;
		
	@Value("${spring.api.resources.urlgroup}") private String urlgroup;
	
	@LocalServerPort private int localport;
		
	private String authenticate() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.add("Authorization", "Basic MU1iOU5HIVRJTDl4TzhmJjp1UzIyQFpsckhiRFVwTmUz");
		headers.add("Cache-Control", "no-cache");
		
		String auth = "username=Peter&password=peter&grant_type=password";
	
		HttpEntity<String> request = new HttpEntity<>(auth, headers);
		ResponseEntity<OAuthModel> postRestTemplate = testRestTemplate.exchange("http://localhost:" + localport + "/" + version + "/oauth/token", HttpMethod.POST, request, OAuthModel.class);
		
		return postRestTemplate.getBody().getAccess_token();
	}
	
	private String authenticateAdmin() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.add("Authorization", "Basic MU1iOU5HIVRJTDl4TzhmJjp1UzIyQFpsckhiRFVwTmUz");
		headers.add("Cache-Control", "no-cache");
		
		String auth = "username=Cgi&password=cgi&grant_type=password";
	
		HttpEntity<String> request = new HttpEntity<>(auth, headers);
		ResponseEntity<OAuthModel> postRestTemplate = testRestTemplate.exchange("http://localhost:" + localport + "/" + version + "/oauth/token", HttpMethod.POST, request, OAuthModel.class);
		
		return postRestTemplate.getBody().getAccess_token();
	}
	
	private String createUrl() {
		logger.info("http://localhost:" + localport + "/" + version + "/" + urlgroup);
		return "http://localhost:" + localport + "/" + version + "/" + urlgroup;
	}
		
	
	/**
	 * setup test environment
	 * 
	 * @throws ClusterMonitoringApplicationException
	 */
	@Before
	public void setUp() throws ClusterMonitoringApplicationException {		
		Set<String> urlSetList = new TreeSet<>();
		urlSetList.add("testGroup");
		
		doNothing().when(redisCRUDRepositoryImpl).flushdb();
		when(redisCRUDRepositoryImpl.readAllUrlGroups()).thenReturn(urlSetList);
		
	}
	
	
	
	/**
	 * /urlgroup
	 * 
	 * get all urlgroups
	 */
	@Test
	public void test_getAllUrlGroups() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<String> request = new HttpEntity<>(headers);
		ResponseEntity<Set<String>> postRestTemplate = testRestTemplate.exchange(createUrl(), HttpMethod.GET, request, new ParameterizedTypeReference<Set<String>>(){});

		assertEquals(HttpStatus.OK, postRestTemplate.getStatusCode());
		assertTrue(postRestTemplate.getBody().contains("testGroup"));
	}

	/**
	 * /urlgroup
	 * 
	 * flushdb
	 */
	@Test
	public void test_flushdb() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticateAdmin());
		
		HttpEntity<String> request = new HttpEntity<>(headers);
		ResponseEntity<RESTFULResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl(), HttpMethod.DELETE, request, RESTFULResponseModel.class);

		verify(redisCRUDRepositoryImpl, times(1)).flushdb();
		assertEquals(HttpStatus.ACCEPTED, postRestTemplate.getStatusCode());
	}
	
	/**
	 * flushdb
	 */
	@Test
	public void test_flushdb_nonAdmin() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<String> request = new HttpEntity<>(headers);
		ResponseEntity<RESTFULResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl(), HttpMethod.DELETE, request, RESTFULResponseModel.class);

		assertEquals(HttpStatus.FORBIDDEN, postRestTemplate.getStatusCode());
	}
}
