package com.probhonjonbaruah.clustermonitoring.exceptions;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.probhonjonbaruah.clustermonitoring.model.RESTFULErrorResponseModel;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
@ResponseBody
public class ControllerErrorHandler  {
	
	/**
	 * handle validation errors
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({ MethodArgumentNotValidException.class })
	public RESTFULErrorResponseModel handleMethodArgumentNotValid( MethodArgumentNotValidException ex) {
		List<String> errors = new ArrayList<>();
		
	    for (FieldError error : ex.getBindingResult().getFieldErrors()) {
	        errors.add(error.getField() + ": " + error.getDefaultMessage());
	    }
	    for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
	        errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
	    }
	    
	    return new RESTFULErrorResponseModel("1001", "ValidationException", errors);
    }
	
	/**
	 * handle conditions if media type is not acceptable
	 * @return
	 */
	@ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
	public RESTFULErrorResponseModel handleHttpMediaTypeNotAcceptableException() {
		List<String> errors = new ArrayList<>();
		errors.add("acceptable MIME type:" + MediaType.APPLICATION_JSON_VALUE);
		
		return new RESTFULErrorResponseModel("1002", "MIME type", errors);
	}

	/**
	 * handle application errors
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(ClusterMonitoringApplicationException.class)
	public RESTFULErrorResponseModel clusterMonitoringApplicationExceptionExceptionHandling(ClusterMonitoringApplicationException ex) {
		return new RESTFULErrorResponseModel(ex.getErrorCode(), ex.getErrorType(), ex.getErrorDescription());
	}
}
