package com.probhonjonbaruah.clustermonitoring.logging;

import java.nio.charset.Charset;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.AbstractStringLayout;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * custom implementation of JSON layout for log4j
 * 
 * @author probhonjon.baruah
 *
 */
@Plugin(name = "CustomJSONLayout", category = "Core", elementType = "layout", printObject = true)
public class Log4jCustomJSONLayout extends AbstractStringLayout {
	
	Logger logger = LogManager.getLogger(getClass());
 
    public Log4jCustomJSONLayout( Charset charset) {
		super(charset);
	}

    /**
     * take input from log4j2 configuration to the class
     * 
     * @param charset
     * @return
     */
	@PluginFactory
    public static Log4jCustomJSONLayout createLayout(@PluginAttribute(value = "charset", defaultString = "UTF-8") Charset charset) {
        return new Log4jCustomJSONLayout(charset);
    }

	/**
	 * all logs come here and are converted to JSON
	 */
	@Override
	public String toSerializable(LogEvent logEvent) {
		
		LoggingModel loggingModel;
		
		loggingModel = new LoggingModel(new Date(logEvent.getTimeMillis()), logEvent.getLevel().toString(), 
                                        logEvent.getThreadName(), logEvent.getLoggerName(), 
                                        logEvent.getMessage().getFormattedMessage());
		
		ObjectMapper toJSON = new ObjectMapper();
		String jsonAsString = "";
		try {
			jsonAsString = toJSON.writeValueAsString(loggingModel);
			
			if (!logEvent.isEndOfBatch()) {
				jsonAsString = jsonAsString + ",\n";
			}
		} catch (JsonProcessingException e) {
			logger.error(e);
		}
		
		return jsonAsString;
	}
	
}	