package com.probhonjonbaruah.clustermonitoring.model;

import java.util.Date;
import java.util.List;

public class RESTFULErrorResponseModel {

	private Date timestamp;
	private String errorCode;
	private String errorType;
	private List<String> errorDescription;
		
	/**
	 * 
	 */
	public RESTFULErrorResponseModel() {
		super();
	}
	
	/**
	 * @param errorCode
	 * @param errorMessage
	 * @param errorDescription
	 */
	public RESTFULErrorResponseModel(String errorCode, String errorType, List<String> errorDescription) {
		super();
		this.timestamp = new Date();
		this.errorCode = errorCode;
		this.errorType = errorType;
		this.errorDescription = errorDescription;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorType() {
		return errorType;
	}

	public List<String> getErrorDescription() {
		return errorDescription;
	}
	
}
