package com.probhonjonbaruah.clustermonitoring.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.FormattedMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.ResourceAccessException;

import com.probhonjonbaruah.clustermonitoring.exceptions.ClusterMonitoringApplicationException;
import com.probhonjonbaruah.clustermonitoring.model.RESTFULErrorResponseModel;
import com.probhonjonbaruah.clustermonitoring.model.RESTFULResponseModel;
import com.probhonjonbaruah.clustermonitoring.model.UrlModel;
import com.probhonjonbaruah.clustermonitoring.model.UrlSetModel;
import com.probhonjonbaruah.clustermonitoring.service.RedisDAOService;

@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestUrlGroupController {
	static final Logger logger = LogManager.getLogger(TestUrlGroupController.class);
	
	@MockBean private RedisDAOService redisDAOService;
	
	@Autowired private TestRestTemplate testRestTemplate;
	
	@LocalServerPort private int localport;
	
	@Value("${spring.api.version}") private String version;
	
	@Value("${spring.api.resources.urlgroup}") private String urlgroup;
	
	private UrlSetModel urlSetModel;
	private List<UrlModel> urlModelList;
	private UrlModel url1;
	private UrlModel url2;
	
	/*
	 * create url by combining values
	 */
	private String createUrl() {
		return "http://localhost:" + localport + "/" + version + "/" + urlgroup;
	}

	private String createUrl(String pathVariable) {
		logger.info(createUrl() + "/" + pathVariable);
		return createUrl() + "/" + pathVariable;
	}
	
	private String authenticate() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.add("Authorization", "Basic MU1iOU5HIVRJTDl4TzhmJjp1UzIyQFpsckhiRFVwTmUz");
		headers.add("Cache-Control", "no-cache");
		
		String auth = "username=Peter&password=peter&grant_type=password";
	
		HttpEntity<String> request = new HttpEntity<>(auth, headers);
		ResponseEntity<OAuthModel> postRestTemplate = testRestTemplate.exchange("http://localhost:" + localport + "/" + version + "/oauth/token", HttpMethod.POST, request, OAuthModel.class);
		
		return postRestTemplate.getBody().getAccess_token();
	}
		
	
	/**
	 * setup test environment
	 * 
	 * @throws ClusterMonitoringApplicationException
	 */
	@Before
	public void setUp() throws ClusterMonitoringApplicationException {
						
		url1 = new UrlModel();
		url1.setUrl("192.168.0.101:8080");
		url1.setTimeout(10);
		url1.setStatus("offline");
		url1.setPriority(0);
		
		url2 = new UrlModel();
		url2.setUrl("192.168.0.102:8081");
		url2.setTimeout(10);
		url2.setStatus("offline");
		url2.setPriority(0);
		
		urlModelList = new ArrayList<>();
		urlModelList.add(url1);
		urlModelList.add(url2);
		
		urlSetModel = new UrlSetModel();
		urlSetModel.setUrlList(urlModelList);
		
		doNothing().when(redisDAOService).createUrlGroup(any(String.class), Matchers.<List<UrlModel>>any());
		when(redisDAOService.getUrlGroup(anyString())).thenReturn(urlModelList);
		doNothing().when(redisDAOService).updateUrlGroup(any(String.class), Matchers.<List<UrlModel>>any());
		doNothing().when(redisDAOService).deleteUrlGroup(any(String.class));
		when(redisDAOService.getUrl(anyString(), anyString())).thenReturn(url1);
		doNothing().when(redisDAOService).deleteUrl(any(String.class), Matchers.<List<UrlModel>>any());
	}
	
	
	
	@Test
	public void test_createUrl() {
		assertNotEquals(0, authenticate().length());
		assertNotEquals(0, localport);
	}
	
	@Test
	public void test_unauthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<UrlSetModel> request = new HttpEntity<>(urlSetModel, headers);
		
		try {
			testRestTemplate.exchange(createUrl("testGroup"), HttpMethod.POST, request, RESTFULResponseModel.class);
			
			fail();
		} catch (ResourceAccessException e) {
			assert(true);
		}
	}
	
	/**
	 * /urlgroup/{urlgroup}
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_createUrlGroup() throws Exception {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<UrlSetModel> request = new HttpEntity<>(urlSetModel, headers);
		ResponseEntity<RESTFULResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl("testGroup"), HttpMethod.POST, request, RESTFULResponseModel.class);

		assertEquals(HttpStatus.CREATED, postRestTemplate.getStatusCode());
		assertEquals("urlgroup", postRestTemplate.getBody().getResourceType().toLowerCase());
		assertEquals("testGroup", postRestTemplate.getBody().getResourceName());
		assertEquals("created", postRestTemplate.getBody().getResponseMessage());
	}
	
	/**
	 * /urlgroup/{urlgroup}
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_createUrlGroup_error() throws Exception {
		/*
		 * override stubbing
		 */
		doThrow(new ClusterMonitoringApplicationException("4001", "database", new FormattedMessage("UrlGroup: {} already exists.", "testGroup")))
			.when(redisDAOService).createUrlGroup(any(String.class), Matchers.<List<UrlModel>>any());
				
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<UrlSetModel> request = new HttpEntity<>(urlSetModel, headers);
		ResponseEntity<RESTFULErrorResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl("testGroup"), HttpMethod.POST, request, RESTFULErrorResponseModel.class);

		assertEquals(HttpStatus.BAD_REQUEST, postRestTemplate.getStatusCode());
		assertEquals("4001", postRestTemplate.getBody().getErrorCode());
		assertEquals("database", postRestTemplate.getBody().getErrorType());
		assertTrue(postRestTemplate.getBody().getErrorDescription().contains("UrlGroup: testGroup already exists."));
	}
	
	/**
	 * /urlgroup/{urlgroup}
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_getUrlGroup() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<String> request = new HttpEntity<>(headers);
		ResponseEntity<List<UrlModel>> postRestTemplate = testRestTemplate.exchange(createUrl("testgroup"), HttpMethod.GET, request, new ParameterizedTypeReference<List<UrlModel>>(){});

		assertEquals(HttpStatus.OK, postRestTemplate.getStatusCode());
		assertEquals(2, postRestTemplate.getBody().size());
		assertTrue(postRestTemplate.getBody().contains(url1));
		assertTrue(postRestTemplate.getBody().contains(url2));
	}
	
	/**
	 * /urlgroup/{urlgroup}
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_getUrlGroup_error() throws Exception {
		/*
		 * override stubbing
		 */
		doThrow(new ClusterMonitoringApplicationException("4011", "database", new FormattedMessage("UrlGroup: {} does not exist", "testGroup")))
			.when(redisDAOService).getUrlGroup(any(String.class));
				
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<String> request = new HttpEntity<>(headers);
		ResponseEntity<RESTFULErrorResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl("testgroup"), HttpMethod.GET, request, RESTFULErrorResponseModel.class);

		assertEquals(HttpStatus.BAD_REQUEST, postRestTemplate.getStatusCode());
		assertEquals("4011", postRestTemplate.getBody().getErrorCode());
		assertEquals("database", postRestTemplate.getBody().getErrorType());
		assertTrue(postRestTemplate.getBody().getErrorDescription().contains("UrlGroup: testGroup does not exist"));
	}
	
	/**
	 * /urlgroup/{urlgroup}
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_putUrlGroup() throws Exception {
		url1 = new UrlModel();
		url1.setUrl("192.168.0.101:8080");
		url1.setTimeout(5);
		url1.setStatus("offline");
		url1.setPriority(0);
		
		url2 = new UrlModel();
		url2.setUrl("192.168.0.102:8081");
		url2.setTimeout(5);
		url2.setStatus("offline");
		url2.setPriority(0);
		
		List<UrlModel> urlModelList = new ArrayList<>();
		urlModelList.add(url1);
		urlModelList.add(url2);
		
		urlSetModel = new UrlSetModel();
		urlSetModel.setUrlList(urlModelList);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<UrlSetModel> request = new HttpEntity<>(urlSetModel, headers);
		ResponseEntity<RESTFULResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl("testGroup"), HttpMethod.PUT, request, RESTFULResponseModel.class);

		verify(redisDAOService, times(1)).updateUrlGroup("testGroup", urlModelList);
		
		assertEquals(HttpStatus.ACCEPTED, postRestTemplate.getStatusCode());
		assertEquals("urlgroup", postRestTemplate.getBody().getResourceType().toLowerCase());
		assertEquals("testGroup", postRestTemplate.getBody().getResourceName());
		assertEquals("updated", postRestTemplate.getBody().getResponseMessage());
	}
	
	/**
	 * /urlgroup/{urlgroup}
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_putUrlGroup_error() throws Exception {
		/*
		 * override stubbing
		 */
		doThrow(new ClusterMonitoringApplicationException("4001", "database", new FormattedMessage("UrlGroup: {} does not exist.", "testGroup")))
			.when(redisDAOService).updateUrlGroup(any(String.class), Matchers.<List<UrlModel>>any());

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<UrlSetModel> request = new HttpEntity<>(urlSetModel, headers);
		ResponseEntity<RESTFULErrorResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl("testGroup"), HttpMethod.PUT, request, RESTFULErrorResponseModel.class);

		assertEquals(HttpStatus.BAD_REQUEST, postRestTemplate.getStatusCode());
		assertEquals("4001", postRestTemplate.getBody().getErrorCode());
		assertEquals("database", postRestTemplate.getBody().getErrorType());
		assertTrue(postRestTemplate.getBody().getErrorDescription().contains("UrlGroup: testGroup does not exist."));
	}
	
	/**
	 * /urlgroup/{urlgroup}
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_deleteUrlGroup() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<String> request = new HttpEntity<>(headers);
		ResponseEntity<RESTFULResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl("testGroup"), HttpMethod.DELETE, request, RESTFULResponseModel.class);

		assertEquals(HttpStatus.OK, postRestTemplate.getStatusCode());
		assertEquals("urlgroup", postRestTemplate.getBody().getResourceType().toLowerCase());
		assertEquals("testGroup", postRestTemplate.getBody().getResourceName());
		assertEquals("deleted", postRestTemplate.getBody().getResponseMessage());
	}
	
	/**
	 * /urlgroup/{urlgroup}
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_deleteUrlGroup_error() throws Exception {
		/*
		 * override stubbing
		 */
		doThrow(new ClusterMonitoringApplicationException("4011", "database", new FormattedMessage("UrlGroup: {} does not exist.", "testGroup")))
			.when(redisDAOService).deleteUrlGroup(any(String.class));
				
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<String> request = new HttpEntity<>(headers);
		ResponseEntity<RESTFULErrorResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl("testgroup"), HttpMethod.DELETE, request, RESTFULErrorResponseModel.class);

		assertEquals(HttpStatus.BAD_REQUEST, postRestTemplate.getStatusCode());
		assertEquals("4011", postRestTemplate.getBody().getErrorCode());
		assertEquals("database", postRestTemplate.getBody().getErrorType());
		assertTrue(postRestTemplate.getBody().getErrorDescription().contains("UrlGroup: testGroup does not exist."));
	}

	
	
	
	/**
	 * /urlgroup/{urlgroup}/url{?port,url}
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_getUrl() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<String> request = new HttpEntity<>(headers);
		ResponseEntity<UrlModel> postRestTemplate = testRestTemplate.exchange(createUrl("testGroup/url?url=192.168.0.101&port=8080"), HttpMethod.GET, request, UrlModel.class);

		assertEquals(HttpStatus.OK, postRestTemplate.getStatusCode());
		assertEquals(url1, postRestTemplate.getBody());
	}
	
	/**
	 * /urlgroup/{urlgroup}/url{?port,url}
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_getUrl_error() throws Exception {
		/*
		 * override stubbing
		 */
		doThrow(new ClusterMonitoringApplicationException("4011", "database", new FormattedMessage("urlgroup: {} does not exist.", "testGroup3")))
			.when(redisDAOService).getUrl(any(String.class), any(String.class));
				
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<String> request = new HttpEntity<>(headers);
		ResponseEntity<RESTFULErrorResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl("testGroup/url?url='192.168.0.101'&port='8080'"), HttpMethod.GET, request, RESTFULErrorResponseModel.class);

		assertEquals(HttpStatus.BAD_REQUEST, postRestTemplate.getStatusCode());
		assertEquals("4011", postRestTemplate.getBody().getErrorCode());
		assertEquals("database", postRestTemplate.getBody().getErrorType());
		assertTrue(postRestTemplate.getBody().getErrorDescription().contains("urlgroup: testGroup3 does not exist."));
	}

	/**
	 * /urlgroup/{urlgroup}/url
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_deleteUrls() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<UrlSetModel> request = new HttpEntity<>(urlSetModel, headers);
		ResponseEntity<RESTFULResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl("testGroup/url"), HttpMethod.DELETE, request, RESTFULResponseModel.class);

		assertEquals(HttpStatus.OK, postRestTemplate.getStatusCode());
		assertEquals("url", postRestTemplate.getBody().getResourceType().toLowerCase());
		assertEquals("testGroup", postRestTemplate.getBody().getResourceName());
		assertEquals("all requested urls deleted", postRestTemplate.getBody().getResponseMessage());
	}
	
	/**
	 * /urlgroup/{urlgroup}/url
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_deleteUrls_error() throws Exception {
		/*
		 * override stubbing
		 */
		doThrow(new ClusterMonitoringApplicationException("4011", "database", new FormattedMessage("UrlGroup: {} does not exist.", "testGroup")))
			.when(redisDAOService).deleteUrl(any(String.class), Matchers.<List<UrlModel>>any());
				
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + authenticate());
		
		HttpEntity<UrlSetModel> request = new HttpEntity<>(urlSetModel, headers);
		ResponseEntity<RESTFULErrorResponseModel> postRestTemplate = testRestTemplate.exchange(createUrl("testGroup/url"), HttpMethod.DELETE, request, RESTFULErrorResponseModel.class);

		assertEquals(HttpStatus.BAD_REQUEST, postRestTemplate.getStatusCode());
		assertEquals("4011", postRestTemplate.getBody().getErrorCode());
		assertEquals("database", postRestTemplate.getBody().getErrorType());
		assertTrue(postRestTemplate.getBody().getErrorDescription().contains("UrlGroup: testGroup does not exist."));
	}

}

class OAuthModel {
	private String access_token;
	private String token_type;
	private String refresh_token;
	private Integer expires_in;
	private String scope;

	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public String getToken_type() {
		return token_type;
	}
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}
	public String getRefresh_token() {
		return refresh_token;
	}
	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
	public Integer getExpires_in() {
		return expires_in;
	}
	public void setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
}
