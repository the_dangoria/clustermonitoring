package com.probhonjonbaruah.clustermonitoring.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;


@Configuration
public class CustomAuthorizationServerConfigurer extends AuthorizationServerConfigurerAdapter {
	
	@Value("${security.oauth2.client.client-id}")
	private String clientId;
	
	@Value("${security.oauth2.client.client-secret}")
	private String clientSecret;
	
	@Value("${security.oauth2.client.authorized-grant-types}")
	private String[] authorizedGrantTypes;
	
	@Value("${security.oauth2.client.scope}")
	private String[] scope;
	
	@Value("${security.oauth2.client.access-token-validity-seconds}")
	private int accessTokenValiditySeconds;
	
	@Value("${security.oauth2.client.refresh-token-validity-seconds}")
	private int refreshTokenValiditySeconds;

	protected AuthenticationManager authenticationManager;

	public CustomAuthorizationServerConfigurer(AuthenticationConfiguration authenticationConfiguration)
					throws Exception {
		this.authenticationManager = authenticationConfiguration.getAuthenticationManager();
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		
		clientSecret = "{noop}" + clientSecret;
		
		clients.inMemory()
			.withClient(clientId)
			.secret(clientSecret)
			.authorizedGrantTypes(authorizedGrantTypes)
			.scopes(scope)
			.accessTokenValiditySeconds(accessTokenValiditySeconds)
			.refreshTokenValiditySeconds(refreshTokenValiditySeconds);

	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.authenticationManager(authenticationManager);
	}
}
