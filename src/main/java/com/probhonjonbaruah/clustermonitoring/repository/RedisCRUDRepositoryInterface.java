package com.probhonjonbaruah.clustermonitoring.repository;

import java.util.Set;

public interface RedisCRUDRepositoryInterface {
	
	/**
	 * 1. get info about all urlsGroups
	 * 
	 * @return
	 */
	public Set<String> readAllUrlGroups();
	
	/**
	 * read all urls
	 */
	public Set<String> readAllUrls();
	
	/**
	 * clear all
	 */
	public void flushdb();
}
