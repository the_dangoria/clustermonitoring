package com.probhonjonbaruah.clustermonitoring.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

/**
 * a model that hold a url's url, priority, status
 * 
 * @author probhonjon.baruah
 * @date 2018.08.31
 */
public class UrlModel {
	
	@Pattern(regexp = "^(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?):(?:6553[0-5]|655[0-2][0-9]\\d|65[0-4](?:\\d){2}|6[0-4](?:\\d){3}|[1-5](?:\\d){4}|[1-9](?:\\d){0,3})$", message="must be a valid URL")
	private String url;
		
	@Min(value = 0) @Max(value = 65536) private Integer priority;
	
	@Pattern(regexp = "^offline|online$", message="status must be either offline or online") private String status;
	
	@Min(value = 0) @Max(value = 65536) private Integer timeout;

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((priority == null) ? 0 : priority.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((timeout == null) ? 0 : timeout.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		return this.hashCode() == ((UrlModel) obj).hashCode();
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

}
