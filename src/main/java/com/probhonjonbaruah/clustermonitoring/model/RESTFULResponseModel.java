package com.probhonjonbaruah.clustermonitoring.model;

import java.util.Date;

public class RESTFULResponseModel {

	private Date timestamp;
	private String resourceName;
	private String resourceType;
	private String responseMessage;

	public RESTFULResponseModel() {
		super();
	}
	
	/**
	 * @param resourceType
	 * @param resourceName
	 * @param responseMessage
	 */
	public RESTFULResponseModel(String resourceType, String resourceName, String responseMessage) {
		super();
		this.timestamp = new Date();
		this.resourceType = resourceType;
		this.resourceName = resourceName;
		this.responseMessage = responseMessage;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public String getResourceType() {
		return resourceType;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	
}
