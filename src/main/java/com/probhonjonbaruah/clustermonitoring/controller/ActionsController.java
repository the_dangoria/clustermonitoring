package com.probhonjonbaruah.clustermonitoring.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.probhonjonbaruah.clustermonitoring.model.RESTFULResponseModel;
import com.probhonjonbaruah.clustermonitoring.repository.RedisCRUDRepositoryImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/${spring.api.resources.urlgroup}", produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
@ResponseStatus(value = HttpStatus.OK)
@CrossOrigin("*")
@Api(tags = "actions")
public class ActionsController {
	
	@Autowired private RedisCRUDRepositoryImpl redisCRUDRepositoryImpl;

	@DeleteMapping
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	@ApiOperation(value = "flush the database")
	public RESTFULResponseModel flushDB() {
		redisCRUDRepositoryImpl.flushdb();
		return new RESTFULResponseModel("Database", "", "flushed");
	}
	
	@GetMapping
	@ApiOperation(value = "retrieve all urlgroups present in the database")
	public Set<String> getAllUrlGroups() {
		return redisCRUDRepositoryImpl.readAllUrlGroups();
	}
}
