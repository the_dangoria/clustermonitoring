package com.probhonjonbaruah.clustermonitoring.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.FormattedMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class AppVariables {
	
	static final Logger logger = LogManager.getLogger(AppVariables.class);
	
	@Value("${spring.application.variables.timeout}")
	private int timeout;
	
	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		
		logger.info(new FormattedMessage("Variable loaded: {}, value: {}", "timeout", timeout));
		
		this.timeout = timeout;
	}

}
