package com.probhonjonbaruah.clustermonitoring.repository;

import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.FormattedMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.probhonjonbaruah.clustermonitoring.helper.AppConstants;
import com.probhonjonbaruah.clustermonitoring.model.UrlModel;


@Repository
public class RedisCRUDRepositoryImpl implements RedisCRUDRepositoryInterface, RedisCRUDRepositoryInterfaceUrl,
				RedisCRUDRepositoryInterfaceUrlset {

	static final Logger logger = LogManager.getLogger(RedisCRUDRepositoryImpl.class);
	
	@Autowired private RedisTemplate<String, String> redisTemplate;
	
	/**
	 * 1. get info about all urlsGroups
	 * 
	 * @return
	 */
	@Override
	public Set<String> readAllUrlGroups() {

		if (logger.isDebugEnabled()) {
			logger.debug(new FormattedMessage("read all urlGroup from urlGroupSet: {}",
							                     AppConstants.GLOBALURLGROUPSETNAME));
		}

		return redisTemplate.opsForSet().members(AppConstants.GLOBALURLGROUPSETNAME);
	}

	/**
	 * read all urls
	 */
	@Override
	public Set<String> readAllUrls() {
		if (logger.isDebugEnabled()) {
			logger.debug(new FormattedMessage("read all urls from urlSet: {}",
											AppConstants.GLOBALURLSETNAME));
		}

		return redisTemplate.opsForSet().members(AppConstants.GLOBALURLSETNAME);
	}
	
	/**
	 * clear all
	 */
	@Override
	public void flushdb() {

		if (logger.isDebugEnabled()) {
			logger.debug(new FormattedMessage("flushing entire database"));
		}

		redisTemplate.getConnectionFactory().getConnection().flushDb();
	}

	/**
	 * 1. add urlGroup to urlGroupSet 2. add url to urlGroup 3. add url as a hashset
	 * 
	 * @param urlGroup
	 * @param url
	 */
	@Override
	public void createUrl(String urlGroup, UrlModel url) {
		redisTemplate.setEnableTransactionSupport(true);

		logger.info(new FormattedMessage("creating url: {} in urlGroup: {}", url.getUrl(), urlGroup));

		redisTemplate.multi();
		redisTemplate.opsForSet().add(AppConstants.GLOBALURLGROUPSETNAME, urlGroup);
		redisTemplate.opsForSet().add(AppConstants.GLOBALURLSETNAME, url.getUrl());
		redisTemplate.opsForSet().add(urlGroup, url.getUrl());
		redisTemplate.opsForHash().put(url.getUrl(), AppConstants.KEYPRIORITY, Integer.toString(url.getPriority()));
		redisTemplate.opsForHash().put(url.getUrl(), AppConstants.KEYSTATUS, url.getStatus());
		redisTemplate.opsForHash().put(url.getUrl(), AppConstants.KEYTIMEOUT, Integer.toString(url.getTimeout()));
		redisTemplate.exec();
	}

	/**
	 * 1. read a url's info
	 * 
	 * @param url
	 * @return
	 */
	@Override
	public UrlModel readUrl(String url) {
		if (logger.isDebugEnabled()) {
			logger.debug(new FormattedMessage("read url: {}", url));
		}

		UrlModel urlModel = null;
		if (redisTemplate.hasKey(url)) {
			Map<Object, Object> urlMap = redisTemplate.opsForHash().entries(url);

			urlModel = new UrlModel();

			urlModel.setUrl(url);
			urlModel.setPriority(Integer.parseInt((String) urlMap.get(AppConstants.KEYPRIORITY)));
			urlModel.setStatus((String) urlMap.get(AppConstants.KEYSTATUS));
			urlModel.setTimeout(Integer.parseInt((String) urlMap.get(AppConstants.KEYTIMEOUT)));
		}

		return urlModel;
	}

	/**
	 * 1. update url details
	 * 
	 * update details about an url from an urlGroup allow only updates to status,
	 * priority and timeout
	 * 
	 * @param urlGroup
	 * @param url
	 */
	@Override
	public void updateUrl(UrlModel url) {
		redisTemplate.setEnableTransactionSupport(true);
		
		if (logger.isDebugEnabled()) {
			logger.debug(new FormattedMessage("updating url: {}", url.getUrl()));
		}

		redisTemplate.multi();
		redisTemplate.opsForHash().put(url.getUrl(), AppConstants.KEYPRIORITY, Integer.toString(url.getPriority()));
		redisTemplate.opsForHash().put(url.getUrl(), AppConstants.KEYSTATUS, url.getStatus());
		redisTemplate.opsForHash().put(url.getUrl(), AppConstants.KEYTIMEOUT, Integer.toString(url.getTimeout()));
		redisTemplate.exec();
	}

	/**
	 * 1. delete url as a hashset 2. delete url from urlGroup
	 * 
	 * @param urlGroup
	 * @param url
	 */
	@Override
	public void deleteUrl(String urlGroup, String url) {
		redisTemplate.setEnableTransactionSupport(true);
		
		logger.info(new FormattedMessage("deleting url: {} in urlGroup: {}", url, urlGroup));

		redisTemplate.multi();
		redisTemplate.delete(url);
		redisTemplate.opsForSet().remove(urlGroup, url);
		redisTemplate.opsForSet().remove(AppConstants.GLOBALURLSETNAME, url);
		redisTemplate.exec();
	}

	/**
	 * 1. get info about all urls in an urlGroup
	 * 
	 * @param urlGroup
	 * @return
	 */
	@Override
	public Set<String> readUrlGroup(String urlGroup) {
		if (logger.isDebugEnabled()) {
			logger.debug(new FormattedMessage("read urlGroup with name: {}", urlGroup));
		}

		return redisTemplate.opsForSet().members(urlGroup);
	}

	/**
	 * 1. delete all urls that are from the urlGroup as hashsets 2. delete urlGroup
	 * as a set 3. delete urlGroup from urlGroupSet
	 * 
	 * @param urlGroup
	 * @param url
	 */
	@Override
	public void deleteUrlGroup(String urlGroup) {
		redisTemplate.setEnableTransactionSupport(true);
		
		Set<String> urlSet = readUrlGroup(urlGroup);

		logger.info(new FormattedMessage("deleting urlGroup with name: {}", urlGroup));

		redisTemplate.multi();
		for (String url : urlSet) {
			redisTemplate.delete(url);
			redisTemplate.opsForSet().remove(AppConstants.GLOBALURLSETNAME, url);
		}
		
		redisTemplate.delete(urlGroup);
		redisTemplate.opsForSet().remove(AppConstants.GLOBALURLGROUPSETNAME, urlGroup);
		redisTemplate.exec();
	}
}
