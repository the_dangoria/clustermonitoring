package com.probhonjonbaruah.clustermonitoring.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.probhonjonbaruah.clustermonitoring.exceptions.ClusterMonitoringApplicationException;
import com.probhonjonbaruah.clustermonitoring.helper.AppVariables;
import com.probhonjonbaruah.clustermonitoring.model.UrlModel;
import com.probhonjonbaruah.clustermonitoring.repository.RedisCRUDRepositoryImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestRedisDAOService {
	
	@SpyBean private AppVariables appVariable; 
		
	@MockBean private RedisCRUDRepositoryImpl redisCRUDRepositoryImpl;
	
	@SpyBean private RedisDAOService redisDAOService;
	
	
	/**
	 * some constants used in the test cases
	 */
	private String urlGroup = "testGroup";
	private String urlGroup1 = "testGroup1";
	private List<UrlModel> urlModelList;
	private List<UrlModel> urlModelList1;
	private UrlModel url1;
	private UrlModel url2;
	private UrlModel url3;
	private UrlModel url4;
	
	
	/**
	 * setup the test cases
	 */
	@Before
	public void setUp() throws Exception {
		/*
		 * redis details
		 */
		
		urlModelList = new ArrayList<UrlModel>();
		urlModelList1 = new ArrayList<UrlModel>();
		
		url1 = new UrlModel();
		url1.setPriority(0);
		url1.setStatus("offline");
		url1.setTimeout(10);
		url1.setUrl("192.168.0.101:8080");
		urlModelList.add(url1);
		
		url2 = new UrlModel();
		url2.setPriority(0);
		url2.setStatus("online");
		url2.setTimeout(10);
		url2.setUrl("192.168.0.102:8081");
		urlModelList.add(url2);
		
		url3 = new UrlModel();
		url3.setPriority(0);
		url3.setStatus("online");
		url3.setTimeout(10);
		url3.setUrl("192.168.0.201:8080");
		urlModelList1.add(url3);
		
		url4 = new UrlModel();
		url4.setPriority(0);
		url4.setStatus("online");
		url4.setTimeout(10);
		url4.setUrl("192.168.0.202:8081");
		urlModelList1.add(url4);
		
		Set<String> urlSet = new TreeSet<String>();
		urlSet.add("192.168.0.101:8080");
		urlSet.add("192.168.0.102:8081");
		
		Set<String> urlSet1 = new TreeSet<String>();
		urlSet1.add("192.168.0.201:8080");
		urlSet1.add("192.168.0.202:8081");
		
		Set<String> urlSetAll = new TreeSet<String>();
		urlSetAll.add("192.168.0.101:8080");
		urlSetAll.add("192.168.0.102:8081");
		urlSetAll.add("192.168.0.201:8080");
		urlSetAll.add("192.168.0.202:8081");
		
		Set<String> urlGroupSet = new TreeSet<String>();
		urlGroupSet.add(urlGroup);
		urlGroupSet.add(urlGroup1);
		
		/*
		 * stubbing
		 */
		doNothing().when(redisCRUDRepositoryImpl).createUrl(any(String.class), any(UrlModel.class));
		when(redisCRUDRepositoryImpl.readUrl("192.168.0.101:8080")).thenReturn(url1);
		when(redisCRUDRepositoryImpl.readUrl("192.168.0.102:8081")).thenReturn(url2);
		when(redisCRUDRepositoryImpl.readUrl("192.168.0.201:8081")).thenReturn(url3);
		when(redisCRUDRepositoryImpl.readUrl("192.168.0.202:8081")).thenReturn(url4);
		when(redisCRUDRepositoryImpl.readUrlGroup(urlGroup)).thenReturn(urlSet);
		when(redisCRUDRepositoryImpl.readUrlGroup(urlGroup1)).thenReturn(urlSet1);
		doNothing().when(redisCRUDRepositoryImpl).updateUrl(any(UrlModel.class));
		doNothing().when(redisCRUDRepositoryImpl).deleteUrl(any(String.class), any(String.class));
		
		when(redisCRUDRepositoryImpl.readAllUrls()).thenReturn(urlSetAll);
		when(redisCRUDRepositoryImpl.readAllUrlGroups()).thenReturn(urlGroupSet);
	}

	
	
	/**
	 * test basic creation of url group
	 * @throws ClusterMonitoringApplicationException 
	 */
	@Test
	public void test_createUrlGroup() throws ClusterMonitoringApplicationException {
		/*
		 * override the above stubbing to show no urlGroup and no url
		 */
		when(redisCRUDRepositoryImpl.readAllUrlGroups()).thenReturn(new TreeSet<String>());
		when(redisCRUDRepositoryImpl.readAllUrls()).thenReturn(new TreeSet<String>());
		
		redisDAOService.createUrlGroup(urlGroup, urlModelList);
		
		verify(redisCRUDRepositoryImpl, times(1)).createUrl(urlGroup, url1);
		verify(redisCRUDRepositoryImpl, times(1)).createUrl(urlGroup, url2);
	}

	
	/**
	 * test creation of url group when it already exists
	 * @throws ClusterMonitoringApplicationException
	 */
	@Test
	public void test_createUrlGroup_groupExists() {
		
		try {
			redisDAOService.createUrlGroup(urlGroup, urlModelList);
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			verify(redisCRUDRepositoryImpl, times(0)).createUrl(any(String.class), any(UrlModel.class));
			
			assertEquals("4001", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertTrue(e.getErrorDescription().contains("UrlGroup: testGroup already exists."));
		}

	}
	
	
	/**
	 * test creation of url group when one url already exists
	 * @throws ClusterMonitoringApplicationException 
	 */
	@Test
	public void test_createUrlGroup_urlExists() {
		/*
		 * override the above stubbing
		 */
		when(redisCRUDRepositoryImpl.readAllUrlGroups()).thenReturn(new TreeSet<String>());
		
		Set<String> urlSet = new TreeSet<String>();
		urlSet.add("192.168.0.101:8080");
		when(redisCRUDRepositoryImpl.readAllUrls()).thenReturn(urlSet);
		
		try {
			redisDAOService.createUrlGroup(urlGroup, urlModelList);
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			verify(redisCRUDRepositoryImpl, times(0)).createUrl(urlGroup, url1);
			verify(redisCRUDRepositoryImpl, times(1)).createUrl(urlGroup, url2);
			
			assertEquals("4002", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertTrue(e.getErrorDescription().contains("Url: 192.168.0.101:8080 already exists."));
		}
	}
	
	
	/**
	 * test creation of url group when one url already exists
	 * @throws ClusterMonitoringApplicationException 
	 */
	@Test
	public void test_createUrlGroup_allUrlExists() {
		/*
		 * override the above stubbing
		 */
		when(redisCRUDRepositoryImpl.readAllUrlGroups()).thenReturn(new TreeSet<String>());
		
		try {
			redisDAOService.createUrlGroup(urlGroup, urlModelList);
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			verify(redisCRUDRepositoryImpl, times(0)).createUrl(urlGroup, url1);
			verify(redisCRUDRepositoryImpl, times(0)).createUrl(urlGroup, url2);
			
			assertEquals("4001", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertEquals("All urls exists so urlGroup: testGroup not created", e.getErrorDescription().get(0));
			assertTrue(e.getErrorDescription().contains("Url: 192.168.0.101:8080 already exists."));
			assertTrue(e.getErrorDescription().contains("Url: 192.168.0.102:8081 already exists."));
		}
	}
	
	
	

	
	/**
	 * test basic updation of url group
	 * @throws ClusterMonitoringApplicationException 
	 */
	@Test
	public void test_updateUrlGroup() throws ClusterMonitoringApplicationException {
		ArrayList<UrlModel> urlModelList1 = new ArrayList<UrlModel>();
		UrlModel url3 = new UrlModel();
		url3.setPriority(0);
		url3.setStatus("online");
		url3.setTimeout(5);
		url3.setUrl("192.168.0.101:8080");
		urlModelList1.add(url3);
		
		redisDAOService.updateUrlGroup(urlGroup, urlModelList1);
		
		verify(redisCRUDRepositoryImpl, times(1)).updateUrl(url3);
	}
	
	
	/**
	 * test basic updation of url group, when url does not exist
	 */
	@Test
	public void test_updateUrlGroup_urlGroupNotExist() {
		ArrayList<UrlModel> urlModelList1 = new ArrayList<UrlModel>();
		UrlModel url3 = new UrlModel();
		url3.setPriority(0);
		url3.setStatus("online");
		url3.setTimeout(5);
		url3.setUrl("192.168.0.101:8080");
		urlModelList1.add(url3);
		
		/*
		 * override stubbing
		 */
		when(redisCRUDRepositoryImpl.readAllUrlGroups()).thenReturn(new TreeSet<String>());
		
		try {
			redisDAOService.updateUrlGroup(urlGroup, urlModelList1);
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			verify(redisCRUDRepositoryImpl, times(0)).updateUrl(url3);
			
			assertEquals("4001", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertTrue(e.getErrorDescription().contains("UrlGroup: testGroup does not exists"));
		}
		
	}
	
	
	/**
	 * test updation of url group, when url exists in another group
	 */
	@Test
	public void test_updateUrlGroup_urlExistsDiffGroup() {
		ArrayList<UrlModel> urlModelList1 = new ArrayList<UrlModel>();
		UrlModel url3 = new UrlModel();
		url3.setPriority(0);
		url3.setStatus("online");
		url3.setTimeout(5);
		url3.setUrl("192.168.0.201:8080");
		urlModelList1.add(url3);
		
		UrlModel url4 = new UrlModel();
		url4.setPriority(0);
		url4.setStatus("online");
		url4.setTimeout(5);
		url4.setUrl("192.168.0.101:8080");
		urlModelList1.add(url4);
				
		try {
			redisDAOService.updateUrlGroup(urlGroup, urlModelList1);
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			verify(redisCRUDRepositoryImpl, times(0)).updateUrl(url3);
			verify(redisCRUDRepositoryImpl, times(1)).updateUrl(url4);
			
			assertEquals("4022", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertTrue(e.getErrorDescription().contains("Url: 192.168.0.201:8080 already exists in another urlGroup."));
		}
	}

	
	
	
	
	/**
	 * test retrieval of an urlGroup info
	 * @throws ClusterMonitoringApplicationException 
	 */
	@Test
	public void test_getUrlGroup() throws ClusterMonitoringApplicationException {
		List<UrlModel> urlModelList = redisDAOService.getUrlGroup(urlGroup);
		
		assertEquals(2, urlModelList.size());
		assertTrue(urlModelList.contains(url1));
		assertTrue(urlModelList.contains(url2));
	}
	
	
	/**
	 * test retrieval of an urlGroup info if urlGroup does not exist
	 */
	@Test
	public void test_getUrlGroup_urlGroupNotExist() {
		when(redisCRUDRepositoryImpl.readUrlGroup(urlGroup)).thenReturn(new TreeSet<String>());
		
		List<UrlModel> urlModelList = new ArrayList<>();
		try {
			urlModelList = redisDAOService.getUrlGroup(urlGroup);
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			
			assertEquals(0, urlModelList.size());
			assertEquals("4011", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertEquals("urlGroup: testGroup does not exist", e.getErrorDescription().get(0));
		}
	}
	
	
	
	
	
	/**
	 * test deletion of an urlGroup
	 * @throws ClusterMonitoringApplicationException 
	 */
	@Test
	public void test_deleteUrlGroup() throws ClusterMonitoringApplicationException {
		redisDAOService.deleteUrlGroup(urlGroup);
		
		verify(redisCRUDRepositoryImpl, times(1)).deleteUrl(urlGroup, "192.168.0.101:8080");
		verify(redisCRUDRepositoryImpl, times(1)).deleteUrl(urlGroup, "192.168.0.102:8081");
	}
	
	
	/**
	 * test deletion of an urlGroup, if urlGroup does not exist
	 */
	@Test
	public void test_deleteUrlGroup_urlGroupNotExist() {
		try {
			redisDAOService.deleteUrlGroup("testGroup3");
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			verify(redisCRUDRepositoryImpl, times(0)).deleteUrl(any(String.class), any(String.class));
			
			assertEquals("4011", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertEquals("UrlGroup: testGroup3 does not exists", e.getErrorDescription().get(0));
		}
		
	}

	
	/**
	 * test retrieval of an url info
	 * @throws ClusterMonitoringApplicationException 
	 */
	@Test
	public void test_getUrl() throws ClusterMonitoringApplicationException {
		UrlModel urlModel = redisDAOService.getUrl(urlGroup, "192.168.0.101:8080");
		
		assertEquals("192.168.0.101:8080", urlModel.getUrl());
	}
	
	
	/**
	 * test retrieval of an url info if urlGroup does not exist
	 */
	@Test
	public void test_getUrl_urlGroupNotExist() {		

		try {
			redisDAOService.getUrl("testGroup3", "192.168.0.101:8080");
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			
			assertEquals("4011", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertEquals("urlgroup: testGroup3 does not exist", e.getErrorDescription().get(0));
		}
	}
	
	/**
	 * test retrieval of an url info if urlGroup does not exist
	 */
	@Test
	public void test_getUrl_urlNotExist() {		

		try {
			redisDAOService.getUrl("testGroup", "192.168.0.201:8080");
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			
			assertEquals("4012", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertEquals("url: 192.168.0.201:8080 does not exist in the urlgroup: testGroup", e.getErrorDescription().get(0));
		}
	}
	
	
	/**
	 * test deletion of an url
	 * @throws ClusterMonitoringApplicationException 
	 */
	@Test
	public void test_deleteUrl() throws ClusterMonitoringApplicationException {
		urlModelList.remove(url2);
		
		redisDAOService.deleteUrl(urlGroup, urlModelList);
		
		verify(redisCRUDRepositoryImpl, times(1)).deleteUrl(urlGroup, "192.168.0.101:8080");
		verify(redisCRUDRepositoryImpl, times(0)).deleteUrl(urlGroup, "192.168.0.102:8081");
	}
	
	/**
	 * test deletion of an url, if urlGroup does not exist
	 */
	@Test
	public void test_deleteUrl_urlGroupNotExist() {
		urlModelList.remove(url2);
		
		try {
			redisDAOService.deleteUrl("testGroup3", urlModelList);
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			verify(redisCRUDRepositoryImpl, times(0)).deleteUrl(any(String.class), any(String.class));
			
			assertEquals("4011", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertEquals("urlgroup: testGroup3 does not exist", e.getErrorDescription().get(0));
		}	
	}
	
	/**
	 * test deletion of an url, if url does not exist
	 */
	@Test
	public void test_deleteUrl_urlGroupNotExist1() {	
		try {
			redisDAOService.deleteUrl("testGroup", urlModelList1);
			
			fail();
		} catch (ClusterMonitoringApplicationException e) {
			verify(redisCRUDRepositoryImpl, times(0)).deleteUrl(any(String.class), any(String.class));
			
			assertEquals("4012", e.getErrorCode().toLowerCase());
			assertEquals("database", e.getErrorType().toLowerCase());
			assertEquals("Url: 192.168.0.201:8080 does not exists.", e.getErrorDescription().get(0));
		}	
	}

}
